<?php

/**
 * @file
 * Adds configuration fields and a block to brand sites which have a Parent Site Name, usually a School or College
 *
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function umass_site_branding_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    case 'help.page.umass_site_branding':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Helps properly brand sites which have a Parent Site Name, usually a School or College') . '</p>';
      return $output;
    }
}


/**
 * Implements hook_form_alter().
 *
 * Adds a new field for Parent Site Name to the Site Information configuration page
 *
 */
function umass_site_branding_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  if ('system_site_information_settings' == $form_id) {

    $config = \Drupal::config('umass.site_branding');

    $form['site_information']['umass_site_branding_site_parent_name'] = [
      '#type' => 'textfield',
      '#title' => t('Parent site name'),
      '#default_value' => $config->get('umass_site_branding_site_parent_name'),
      '#description' => t('UMass site branding: The Parent site name, usually blank.'),
      '#maxlength' => 255,
    ];

    $form['site_information']['umass_site_branding_site_parent_url'] = [
      '#type'          => 'url',
      '#title'         => t('Parent site URL'),
      '#default_value' => $config->get('umass_site_branding_site_parent_url'),
      '#description'   => t("UMass site branding: The absolute Parent site URL, usually blank, e.g. 'https://www.umass.edu/sbs/'"),
      '#maxlength' => 1024,
    ];

    $form['#submit'][] = 'umass_site_branding_site_parent_name_handler';
  }
}

/**
 * Form submit handler for umass_site_branding_form_alter()
 */
function umass_site_branding_site_parent_name_handler(&$form, FormStateInterface $form_state) {
  $config = \Drupal::service('config.factory')->getEditable('umass.site_branding');

  $config->set('umass_site_branding_site_parent_name', $form_state->getValue('umass_site_branding_site_parent_name'))->save();
  $config->set('umass_site_branding_site_parent_url', $form_state->getValue('umass_site_branding_site_parent_url'))->save();

}

/**
 * Implements hook_theme().
 *
 */
function umass_site_branding_theme($existing, $type, $theme, $path) {
  return [
    'block__umass_site_branding' => [
      'template' => 'block--umass-site-branding',
    ]
  ];
}

function umass_site_branding_preprocess_block__umass_site_branding(&$variables) {
  global $base_url;
  // dpm($variables);

  $variables['#attached']['library'][] = 'umass_site_branding/css';

  $variables['site_name'] = \Drupal::config('system.site')->get('name');
  $variables['site_url'] = $base_url;

  $config = \Drupal::config('umass.site_branding');

  $variables['site_parent_name'] = $config->get('umass_site_branding_site_parent_name');
  $variables['site_parent_url'] = $config->get('umass_site_branding_site_parent_url');

  // find another way to access this. This feels awkward
  $configuration = $variables['elements']['#configuration'];

  $variables['umass_site_branding_option_1'] = $configuration['umass_site_branding_option_1'];
  $variables['umass_site_branding_option_2'] = $configuration['umass_site_branding_option_2'];
  $variables['umass_site_branding_option_3'] = $configuration['umass_site_branding_option_3'];
}
